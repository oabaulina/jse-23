package ru.baulina.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestName;
import ru.baulina.tm.IntegrationTest;
import ru.baulina.tm.util.HashUtil;

@Category(IntegrationTest.class)
public class UserEndpointIT {

    @NotNull private static Session sessionTest;
    @NotNull private static Session sessionAdmin;

    @Rule
    public final TestName testName = new TestName();

    @NotNull
    private static final SessionEndpointService sessionEndpointService = new SessionEndpointService();
    @NotNull
    private static final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();
    @NotNull
    private final UserEndpointService userEndpointService = new UserEndpointService();
    @NotNull
    private final UserEndpoint userEndpoint = userEndpointService.getUserEndpointPort();
    @NotNull
    private final AdminUserEndpointService adminUserEndpointService = new AdminUserEndpointService();
    @NotNull
    private final AdminUserEndpoint adminUserEndpoint = adminUserEndpointService.getAdminUserEndpointPort();

    @Before
    public void setTestName() throws Exception {
        System.out.println(testName.getMethodName());
    }

    @AfterClass
    public static void setUsers() throws Exception {
        sessionEndpoint.closeSession(sessionTest);
        sessionEndpoint.closeSession(sessionAdmin);
    }

    @BeforeClass
    public static void setSessions() throws Exception {
        sessionTest = sessionEndpoint.openSession("test", "test");
        sessionAdmin = sessionEndpoint.openSession("admin", "admin");
     }

    @Test
    public void testChangeUserPassword() {
        @NotNull User user = userEndpoint.createUser(sessionAdmin, "user", "userPassword" );
        Assert.assertNotNull(user);
        @NotNull Session sessionUser = sessionEndpoint.openSession("user", "userPassword");
        userEndpoint.changeUserPassword(
                sessionUser, "userPassword", "passwordNew", sessionUser.getUserId()
        );
        @NotNull Session sessionUserNewPassword = sessionEndpoint.openSession("user", "passwordNew");
        Assert.assertNotNull(sessionUserNewPassword);
        sessionEndpoint.closeSession(sessionUser);
        sessionEndpoint.closeSession(sessionUserNewPassword);
        adminUserEndpoint.removeUserByLogin(sessionAdmin, "user");
    }

    @Test
    public void testProfileOfUserChange() {
        @NotNull User user = userEndpoint.createUser(sessionAdmin, "user", "userPassword" );
        Assert.assertNotNull(user);
        @NotNull Session sessionUser = sessionEndpoint.openSession("user", "userPassword");
        userEndpoint.profileOfUserChange(
                sessionUser, "test@test.ru", "Petr",
                "Ivanov", sessionUser.getUserId()
        );
        @NotNull User actualUser = adminUserEndpoint.findUserByLogin(sessionAdmin, "user");
        Assert.assertEquals("test@test.ru", actualUser.getEmail());
        Assert.assertEquals("Petr", actualUser.getFirstName());
        Assert.assertEquals("Ivanov", actualUser.getLastName());
        sessionEndpoint.closeSession(sessionUser);
        adminUserEndpoint.removeUserByLogin(sessionAdmin, "user");
    }

    @Test
    public void testCreateUserWithRole() {
        @NotNull User newUser = userEndpoint.createUserWithRole(
                sessionAdmin, "newUser", "newUser", Role.USER
        );
        Assert.assertNotNull(newUser);
        Assert.assertEquals("newUser", newUser.getLogin());
        Assert.assertEquals( HashUtil.salt("newUser"), newUser.getPasswordHash());
        Assert.assertEquals(Role.USER, newUser.getRole());
    }

    @Test
    public void testCreateUser() {
        @NotNull User newUser2 = userEndpoint.createUser(sessionAdmin, "newUser", "newUserPassword" );
        Assert.assertNotNull(newUser2);
        Assert.assertEquals("newUser", newUser2.getLogin());
        sessionEndpoint.openSession("newUser", "newUserPassword");
    }

    @Test
    public void testCreateUserWithEmail() {
        @NotNull User newUser3 = userEndpoint.createUserWithEmail(
                sessionAdmin, "newUser", "newUserPassword", "nweUser@user.ru"
        );
        Assert.assertNotNull(newUser3);
        Assert.assertEquals("newUser", newUser3.getLogin());
        Assert.assertEquals("nweUser@user.ru", newUser3.getEmail());
        sessionEndpoint.openSession("newUser", "newUserPassword");
    }
}