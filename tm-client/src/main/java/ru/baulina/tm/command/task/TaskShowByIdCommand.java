package ru.baulina.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.endpoint.Session;
import ru.baulina.tm.endpoint.Task;
import ru.baulina.tm.util.TerminalUtil;

public final class TaskShowByIdCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String name() {
        return "task-show-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "Show task by id.";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER ID");
        @Nullable final Long id = TerminalUtil.nexLong();
        @Nullable final Session session = getSession();
        @Nullable final Task task = endpointLocator.getTaskEndpoint().findOneTaskById(session, id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("[OK]");
        System.out.println();
    }

}
