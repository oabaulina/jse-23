package ru.baulina.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.baulina.tm.command.AbstractCommand;

import java.util.List;

public final class HelpCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-h";
    }

    @NotNull
    @Override
    public String name() {
        return "help";
    }

    @NotNull
    @Override
    public String description() {
        return "Display list of terminal commands.";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        @NotNull final List<AbstractCommand> commands = serviceLocator.getCommandService().findAll();
        for (final AbstractCommand command: commands) System.out.println(command);
        System.out.println("[OK]");
        System.out.println();
    }

}
