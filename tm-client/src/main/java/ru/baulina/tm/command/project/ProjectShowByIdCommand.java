package ru.baulina.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.endpoint.Project;
import ru.baulina.tm.endpoint.Session;
import ru.baulina.tm.util.TerminalUtil;

public final class ProjectShowByIdCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String name() {
        return "project-show-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "Show project by id.";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER ID");
        @Nullable final Long id = TerminalUtil.nexLong();
        @Nullable final Session session = getSession();
        @Nullable final Project project = endpointLocator.getProjectEndpoint().findOneProjectById(
                session, session.getUserId()
        );
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("[OK]");
        System.out.println();
    }

}
