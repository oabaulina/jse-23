package ru.baulina.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.endpoint.Project;
import ru.baulina.tm.endpoint.Session;

import java.util.List;

public final class ProjectShowCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String name() {
        return "project-list";
    }

    @NotNull
    @Override
    public String description() {
        return "Show task project.";
    }

    @Override
    public void execute() {
        System.out.println("[LIST PROJECT]");
        @Nullable final Session session = getSession();
        @Nullable final List<Project> projects = endpointLocator.getProjectEndpoint().findAllProjects(session);
        int index = 1;
        for (Project project: projects) {
            System.out.println(index + ". " + project);
            index++;
        }
        System.out.println("[OK]");
        System.out.println();
    }

}
