package ru.baulina.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.endpoint.Project;
import ru.baulina.tm.endpoint.Session;
import ru.baulina.tm.util.TerminalUtil;

public final class ProjectShowByNameCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String name() {
        return "project-show-by-name";
    }

    @NotNull
    @Override
    public String description() {
        return "Show project by name.";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER NAME");
        @Nullable final String name = TerminalUtil.nextLine();
        @Nullable final Session session = getSession();
        @Nullable final Project project = endpointLocator.getProjectEndpoint().removeOneProjectByName(session, name);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("[OK]");
        System.out.println();
    }

}
