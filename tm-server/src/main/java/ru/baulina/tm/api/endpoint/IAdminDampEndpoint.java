package ru.baulina.tm.api.endpoint;

import ru.baulina.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IAdminDampEndpoint {

    @WebMethod
    void dataBase64Clear(
            @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    void dataBase64Load(
            @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    void dataBase64Save(
            @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    void dataBinaryClear(
            @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    void dataBinaryLoad(
            @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    void dataBinarySave(
            @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    void dataJasonClear(
            @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    void dataJasonLoad(
            @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    void dataJasonSave(
            @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    void dataXmlClear(
            @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    void dataXmlLoad(
            @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    void dataXmlSave(
            @WebParam(name = "session", partName = "session") Session session
    );

}
