package ru.baulina.tm.api.endpoint;

import ru.baulina.tm.entity.Project;
import ru.baulina.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IProjectEndpoint {

    @WebMethod
    List<Project> getProjectList();

    @WebMethod
    void loadProjects(
            @WebParam(name = "entities", partName = "entities") List<Project> projects
    );

    @WebMethod
    Project createProject(
            @WebParam(name = "session", partName = "session") Session session,
            @WebParam(name = "name", partName = "name") String name
    );

    @WebMethod
    Project createProjectWithDescription(
            @WebParam(name = "session", partName = "session") Session session,
            @WebParam(name = "name", partName = "name") String name,
            @WebParam(name = "description", partName = "description") String description
    );

    @WebMethod
    Project addProject(
            @WebParam(name = "session", partName = "session") Session session,
            @WebParam(name = "project", partName = "project") Project project
    );

    @WebMethod
    void removeProject(
            @WebParam(name = "session", partName = "session") Session session,
            @WebParam(name = "project", partName = "project") Project project
    );

    @WebMethod
    List<Project> findAllProjects(
            @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    void clearProjects(
            @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    Project findOneProjectById(
            @WebParam(name = "session", partName = "session") Session session,
            @WebParam(name = "id", partName = "id") Long id
    );

    @WebMethod
    Project findOneProjectByIndex(
            @WebParam(name = "session", partName = "session") Session session,
            @WebParam(name = "index", partName = "index") Integer index
    );

    @WebMethod
    Project findOneProjectByName(
            @WebParam(name = "session", partName = "session") Session session,
            @WebParam(name = "name", partName = "name") String name
    );

    @WebMethod
    Project removeOneProjectById(
            @WebParam(name = "session", partName = "session") Session session,
            @WebParam(name = "id", partName = "id") Long id
    );

    @WebMethod
    Project removeOneProjectByIndex(
            @WebParam(name = "session", partName = "session") Session session,
            @WebParam(name = "index", partName = "index") Integer index
    );

    @WebMethod
    Project removeOneProjectByName(
            @WebParam(name = "session", partName = "session") Session session,
            @WebParam(name = "name", partName = "name") String name
    );

    @WebMethod
    Project updateProjectById(
            @WebParam(name = "session", partName = "session") Session session,
            @WebParam(name = "id", partName = "id") Long id,
            @WebParam(name = "name", partName = "name") String name,
            @WebParam(name = "description", partName = "description") String description
    );

    @WebMethod
    Project updateProjectByIndex(
            @WebParam(name = "session", partName = "session") Session session,
            @WebParam(name = "index", partName = "index") Integer index,
            @WebParam(name = "name", partName = "name") String name,
            @WebParam(name = "description", partName = "description") String description
    );

}
