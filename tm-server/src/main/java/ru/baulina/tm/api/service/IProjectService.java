package ru.baulina.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.entity.Project;

import java.util.List;

public interface IProjectService extends IService<Project> {

    @Nullable
    Project create(@Nullable final Long userId, @Nullable final String name);

    @Nullable
    Project create(
            @Nullable final Long userId, @Nullable final String name,
            @Nullable final String description
    );

    @Nullable
    Project add(@Nullable final Long userId, @Nullable final Project project);

    void remove(@Nullable final Long userId, @Nullable final Project project);

    @NotNull
    List<Project> findAll(@Nullable final Long userId);

    void clear(@Nullable final Long userId);

    @Nullable
    Project findOneById(@Nullable final Long userId, @Nullable final Long id);

    @Nullable
    Project findOneByIndex(@Nullable final Long userId, @Nullable final Integer index);

    @Nullable
    Project findOneByName(@Nullable final Long userId, @Nullable final String name);

    @Nullable
    Project removeOneById(@Nullable final Long userId, @Nullable final Long id);

    @Nullable
    Project removeOneByIndex(@Nullable final Long userId, @Nullable final Integer index);

    @Nullable
    Project removeOneByName(@Nullable final Long userId, @Nullable final String name);

    @Nullable
    Project updateProjectById(
            @Nullable final Long userId, @Nullable final Long id,
            @Nullable final String name, @Nullable final String  description
    );

    @Nullable
    Project updateProjectByIndex(
            @Nullable final Long userId, @Nullable final Integer index,
            @Nullable final String name, @Nullable final String  description
    );

}
