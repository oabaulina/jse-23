package ru.baulina.tm.api.endpoint;

import ru.baulina.tm.entity.Session;
import ru.baulina.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface ISessionEndpoint {

    @WebMethod
    Session openSession(
            @WebParam(name = "login", partName = "login") String login,
            @WebParam(name = "password", partName = "password") String password
    );

    @WebMethod
    void closeSession(
            @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    void closeAllSession(
            @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    User getUser(
            @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    List<Session> getListSession(
            @WebParam(name = "session", partName = "session") Session session
    );

}
