package ru.baulina.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.baulina.tm.entity.Project;
import ru.baulina.tm.entity.Session;
import ru.baulina.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface ITaskEndpoint {

    @WebMethod
    @NotNull List<Task> getTaskList();

    @WebMethod
    void loadTasks(
            @WebParam(name = "tasks", partName = "tasks") List<Task> tasks
    );

    @WebMethod
    Task createTask(
            @WebParam(name = "session", partName = "session") Session session,
            @WebParam(name = "name", partName = "name") String name
    );

    @WebMethod
    Task createTaskWithDescription(
            @WebParam(name = "session", partName = "session") Session session,
            @WebParam(name = "name", partName = "name") String name,
            @WebParam(name = "description", partName = "description") String description
    );

    @WebMethod
    Task addTask(
            @WebParam(name = "session", partName = "session") Session session,
            @WebParam(name = "task", partName = "task") Task task
    );

    @WebMethod
    void removeTask(
            @WebParam(name = "session", partName = "session") Session session,
            @WebParam(name = "task", partName = "task") Task task
    );

    @WebMethod
    List<Task> findAllTasks(
            @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    void clearTasks(
            @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    Task findOneTaskById(
            @WebParam(name = "session", partName = "session") Session session,
            @WebParam(name = "id", partName = "id") Long id
    );

    @WebMethod
    Task findOneTaskByIndex(
            @WebParam(name = "session", partName = "session") Session session,
            @WebParam(name = "index", partName = "index") Integer index
    );

    @WebMethod
    Task findOneTaskByName(
            @WebParam(name = "session", partName = "session") Session session,
            @WebParam(name = "name", partName = "name") String name
    );

    @WebMethod
    Task removeOneTaskById(
            @WebParam(name = "session", partName = "session") Session session,
            @WebParam(name = "id", partName = "id") Long id
    );

    @WebMethod
    Task removeOneTaskByIndex(
            @WebParam(name = "session", partName = "session") Session session,
            @WebParam(name = "index", partName = "index") Integer index
    );

    @WebMethod
    Task removeOneTaskByName(
            @WebParam(name = "session", partName = "session") Session session,
            @WebParam(name = "name", partName = "name") String name
    );

    @WebMethod
    Task updateTaskById(
            @WebParam(name = "session", partName = "session") Session session,
            @WebParam(name = "id", partName = "id") Long id,
            @WebParam(name = "name", partName = "name") String name,
            @WebParam(name = "description", partName = "description") String description
    );

    @WebMethod
    Task updateTaskByIndex(
            @WebParam(name = "session", partName = "session") Session session,
            @WebParam(name = "index", partName = "index") Integer index,
            @WebParam(name = "name", partName = "name") String name,
            @WebParam(name = "description", partName = "description") String description
    );

}
