package ru.baulina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.api.repository.ITaskRepository;
import ru.baulina.tm.api.service.ITaskService;
import ru.baulina.tm.entity.Task;
import ru.baulina.tm.exception.empty.EmptyIdException;
import ru.baulina.tm.exception.empty.EmptyNameException;
import ru.baulina.tm.exception.empty.EmptyPasswordException;
import ru.baulina.tm.exception.empty.EmptyUserIdException;
import ru.baulina.tm.exception.incorrect.IncorrectIndexException;

import java.util.List;

public final class TaskService extends AbstractService<Task> implements ITaskService {

    @NotNull private final ITaskRepository taskRepository;

    public TaskService(@NotNull final ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @Override
    public Task create(@Nullable final Long userId, @Nullable final String name) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Task task = new Task();
        task.setName(name);
        taskRepository.add(userId, task);
        return task;
    }

    @Override
    public Task create(
            @Nullable final Long userId, @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId < 0) throw new EmptyPasswordException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) return null;
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(userId, task);
        return task;
    }

    @Override
    public Task add(@Nullable final Long userId, @Nullable final Task task) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (task == null) return null;
        taskRepository.add(userId, task);
        return task;
    }

    @Override
    public void remove(@Nullable final Long userId, @Nullable final Task task) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (task == null) return;
        taskRepository.remove(userId, task);
    }

    @NotNull
    @Override
    public List<Task> findAll(@Nullable final Long userId) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
         return taskRepository.findAll(userId);
    }

    @Override
    public void clear(@Nullable final Long userId) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        taskRepository.clear(userId);
    }

    @Nullable
    @Override
    public Task findOneById(@Nullable final Long userId, @Nullable final Long id) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (id == null || id < 0) throw new EmptyIdException();
        return taskRepository.findOneById(userId, id);
    }

    @Nullable
    @Override
    public Task findOneByIndex(@Nullable final Long userId, @Nullable final Integer index) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        return taskRepository.findOneByIndex(userId, index);
    }

    @Nullable
    @Override
    public Task findOneByName(@Nullable final Long userId, @Nullable final String name) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.findOneByName(userId, name);
    }

    @Nullable
    @Override
    public Task removeOneById(@Nullable final Long userId, @Nullable final Long id) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
         if (id == null || id < 0) throw new EmptyIdException();
         return taskRepository.removeOneById(userId, id);
    }

    @Nullable
    @Override
    public Task removeOneByIndex(@Nullable final Long userId, @Nullable final Integer index) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        return taskRepository.removeOneByIndex(userId, index);
    }

    @Nullable
    @Override
    public Task removeOneByName(@Nullable final Long userId, @Nullable final String name) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.removeOneByName(userId, name);
    }

    @Nullable
    @Override
    public Task updateTaskById(
            @Nullable final Long userId, @Nullable final Long id,
            @Nullable final String name, @Nullable final String description
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (id == null || id < 0) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneById(userId, id);
        if (task == null) return null;
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        taskRepository.merge(task);
        return task;
    }

    @Nullable
    @Override
    public Task updateTaskByIndex(
            @Nullable final Long userId, @Nullable final Integer index,
            @Nullable final String name, @Nullable final String description
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final Task task = findOneByIndex(userId, index);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        taskRepository.merge(task);
        return task;
    }

}
