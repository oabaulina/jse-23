package ru.baulina.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.api.repository.IProjectRepository;
import ru.baulina.tm.entity.Project;
import ru.baulina.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public void add(@NotNull final Long userId, @NotNull final Project project) {
        project.setUserId(userId);
        merge(project);
    }

    @Override
    public void remove(@NotNull final Long userId, @NotNull final Project project) {
        @NotNull final List<Project> projects = getList();
        @NotNull Long id = project.getId();
        @Nullable final Project projectDeleted = findOneById(userId, id);
        projects.remove(projectDeleted);
    }

    @Override
    public void clear(@NotNull final Long userId) {
        @NotNull final List<Project> projects = getList();
        projects.removeIf(project -> userId.equals(project.getUserId()));
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull final Long userId) {
        @NotNull final List<Project> result = new ArrayList<>();
        @NotNull final List<Project> projects = getList();
        for (@NotNull final Project project: projects) {
            if (userId.equals(project.getUserId())) result.add(project);
        }
        return result;
    }

    @Nullable
    @Override
    public Project findOneById(@NotNull final Long userId, @NotNull final Long id) {
        @NotNull final List<Project> projects = getList();
        for (@NotNull final Project project: projects) {
            if (userId.equals(project.getUserId()) && id.equals(project.getId())) return project;
        }
        return null;
    }

    @Nullable
    @Override
    public Project findOneByIndex(@NotNull final Long userId, @NotNull final Integer index) {
        @NotNull final List<Project> result = new ArrayList<>();
        @NotNull final List<Project> projects = getList();
        for (@NotNull final Project project: projects) {
            if (userId.equals(project.getUserId())) result.add(project);
        }
        return result.get(index);
    }

    @Nullable
    @Override
    public Project findOneByName(@NotNull final Long userId, @NotNull final String name) {
        @NotNull final List<Project> projects = getList();
        for (@NotNull final Project project: projects) {
            if (userId.equals(project.getUserId()) && name.equals(project.getName())) return project;
        }
        return null;
    }

    @Nullable
    @Override
    public Project removeOneById(@NotNull final Long userId, @NotNull final Long id) {
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) return null;
        @NotNull final List<Project> projects = getList();
        projects.remove(project);
        return project;
    }

    @Nullable
    @Override
    public Project removeOneByIndex(@NotNull final Long userId, @NotNull final Integer index) {
        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) return null;
        remove(userId, project);
        return project;
    }

    @Nullable
    @Override
    public Project removeOneByName(@NotNull final Long userId, @NotNull final String name) {
        @Nullable final Project project = findOneByName(userId, name);
        if (project == null) return null;
        remove(userId, project);
        return project;
    }

}
