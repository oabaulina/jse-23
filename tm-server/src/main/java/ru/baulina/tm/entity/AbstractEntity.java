package ru.baulina.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Random;

@Getter
@Setter
@XmlRootElement
@NoArgsConstructor
public abstract class AbstractEntity implements Serializable {

    private long id = Math.abs(new Random().nextLong());

}
