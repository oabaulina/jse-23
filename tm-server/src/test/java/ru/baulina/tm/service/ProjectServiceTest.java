package ru.baulina.tm.service;

import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestName;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import ru.baulina.tm.UnitServiceTest;
import ru.baulina.tm.UnitTest;
import ru.baulina.tm.api.repository.IProjectRepository;
import ru.baulina.tm.api.service.IProjectService;
import ru.baulina.tm.entity.Project;
import ru.baulina.tm.entity.User;
import ru.baulina.tm.enumerated.Role;
import ru.baulina.tm.exception.empty.EmptyIdException;
import ru.baulina.tm.exception.empty.EmptyNameException;
import ru.baulina.tm.exception.empty.EmptyUserIdException;
import ru.baulina.tm.exception.incorrect.IncorrectIndexException;

import java.util.ArrayList;

import static org.mockito.Mockito.*;

@Category({UnitTest.class, UnitServiceTest.class})
public class ProjectServiceTest {

    @Nullable private static Project project;
    @Nullable private static User userTest;

    @Rule
    @Nullable
    public final TestName testName = new TestName();

    IProjectRepository mProjectRepository = Mockito.mock(IProjectRepository.class);

    @InjectMocks
    private final IProjectService projectService = new ProjectService(mProjectRepository);

    @Before
    public void setTestName() throws Exception {
        System.out.println(testName.getMethodName());
    }

    @BeforeClass
    public static void setUsers() throws Exception {
        userTest = new User();
        userTest.setId(12345L);
        userTest.setLogin("test");
        userTest.setPasswordHash("14jhd54");
        userTest.setRole(Role.USER);

        project = new Project();
        project.setId(22222L);
        project.setUserId(12345L);
        project.setName("project");
    }

    @Test (expected = EmptyUserIdException.class)
    public void testCreateWithNullUserId() {
        projectService.create(null, "project");
        projectService.create(null, "project", "description");
    }

    @Test (expected = EmptyNameException.class)
    public void testCreateWithNullName() {
        projectService.create(123L, null);
        projectService.create(123L, null, "description");
    }

    @Test (expected = EmptyNameException.class)
    public void testCreateWithEmptyName() {
        projectService.create(123L, "");
        projectService.create(123L, "", "description");
    }

    @Test
    public void testCreate() {
        doNothing().when(mProjectRepository).add(eq(123L), isA(Project.class));
        projectService.create(123L, "newProject");
        ArgumentCaptor<Project> projectCaptor = ArgumentCaptor.forClass(Project.class);
        verify(mProjectRepository).add(eq(123L), projectCaptor.capture());
        Project createdProject = projectCaptor.getValue();
        Assert.assertEquals("newProject", createdProject.getName());
    }

    @Test
    public void testCreateWithDescription() {
        doNothing().when(mProjectRepository).add(eq(12345L), isA(Project.class));
        projectService.create(12345L, "newProject","description");
        ArgumentCaptor<Project> projectCaptor = ArgumentCaptor.forClass(Project.class);
        verify(mProjectRepository).add(eq(12345L), projectCaptor.capture());
        Project createdProject = projectCaptor.getValue();
        Assert.assertEquals("newProject", createdProject.getName());
        Assert.assertEquals("description", createdProject.getDescription());
    }

    @Test (expected = EmptyUserIdException.class)
    public void testAddWithNullUserId() {
        projectService.add(null, project);
    }

    @Test
    public void testAdd() {
        doNothing().when(mProjectRepository).add(12345L, project);
        projectService.add(12345L, project);
        verify(mProjectRepository).add(12345L, project);
    }

    @Test (expected = EmptyUserIdException.class)
    public void testRemoveWithNullUserId() {
        projectService.remove(null, project);
    }

    @Test
    public void testRemove() {
        doNothing().when(mProjectRepository).remove(12345L, project);
        projectService.remove(12345L, project);
        verify(mProjectRepository).remove(12345L, project);
    }

    @Test (expected = EmptyUserIdException.class)
    public void testFindAllWithNullUserId() {
        projectService.findAll(null);
    }

    @Test
    public void testFindAll() {
        @Nullable ArrayList<Project> projects = new ArrayList<>();
        projects.add(project);
        Project project1 = new Project();
        project1.setUserId(12345L);
        project1.setName("project");
        projects.add(project1);
        when(mProjectRepository.findAll(12345L)).thenReturn(projects);
        Assert.assertEquals(projects, projectService.findAll(12345L));
        verify(mProjectRepository).findAll(12345L);
    }

    @Test (expected = EmptyUserIdException.class)
    public void testClearWithNullUserId() {
        projectService.clear(null);
    }

    @Test
    public void testClear() {
        doNothing().when(mProjectRepository).clear(12345L);
        projectService.clear(12345L);
        verify(mProjectRepository).clear(12345L);
    }

    @Test (expected = EmptyUserIdException.class)
    public void testFindOneWithNullUserIdById() {
        projectService.findOneById(null, 123L);
    }

    @Test (expected = EmptyIdException.class)
    public void testFindOneWithNullIdById() {
        projectService.findOneById(123L, null);
    }

    @Test
    public void testFindOneById() {
        when(mProjectRepository.findOneById(12345L, 22222L)).thenReturn(project);
        Assert.assertEquals(project, projectService.findOneById(12345L, 22222L));
        verify(mProjectRepository).findOneById(12345L, 22222L);
    }

    @Test (expected = EmptyUserIdException.class)
    public void testFindOneWithNullUserIdByIndex() {
        projectService.findOneByIndex(null, 0);
    }

    @Test (expected = IncorrectIndexException.class)
    public void testFindOneWithNullIndexByIndex() {
        projectService.findOneByIndex(123L, null);
    }

    @Test
    public void testFindOneByIndex() {
        when(mProjectRepository.findOneByIndex(12345L, 0)).thenReturn(project);
        Assert.assertEquals(project, projectService.findOneByIndex(12345L, 0));
        verify(mProjectRepository).findOneByIndex(12345L, 0);
    }

    @Test (expected = EmptyUserIdException.class)
    public void testFindOneWithNullUserIdByName() {
        projectService.findOneByName(null, "project");
    }

    @Test (expected = EmptyNameException.class)
    public void testFindOneWithNullNameByName() {
        projectService.findOneByName(123L, null);
    }

    @Test
    public void testFindOneByName() {
        when(mProjectRepository.findOneByName(12345L, "project")).thenReturn(project);
        Assert.assertEquals(project, projectService.findOneByName(12345L, "project"));
        verify(mProjectRepository).findOneByName(12345L, "project");
    }

    @Test (expected = EmptyUserIdException.class)
    public void testRemoveOneWithNullUserIdById() {
        projectService.removeOneById(null, 123L);
    }

    @Test (expected = EmptyIdException.class)
    public void testRemoveOneWithNullIdById() {
        projectService.removeOneById(123L, null);
    }

    @Test
    public void testRemoveOneById() {
        when(mProjectRepository.removeOneById(12345L, 22222L)).thenReturn(project);
        Assert.assertEquals(project, projectService.removeOneById(12345L, 22222L));
        verify(mProjectRepository).removeOneById(12345L, 22222L);
    }

    @Test (expected = EmptyUserIdException.class)
    public void testRemoveOneWithNullUserIdByIndex() {
        projectService.removeOneByIndex(null, 0);
    }

    @Test (expected = IncorrectIndexException.class)
    public void testRemoveOneWithNullIndexByIndex() {
        projectService.removeOneByIndex(123L, null);
    }

    @Test
    public void testRemoveOneByIndex() {
        when(mProjectRepository.removeOneByIndex(12345L, 0)).thenReturn(project);
        Assert.assertEquals(project, projectService.removeOneByIndex(12345L, 0));
        verify(mProjectRepository).removeOneByIndex(12345L, 0);
    }

    @Test (expected = EmptyUserIdException.class)
    public void testRemoveOneWithNullUserIdByName() {
        projectService.removeOneByName(null, "project");
    }

    @Test (expected = EmptyNameException.class)
    public void testRemoveOneWithNullNameByName() {
        projectService.removeOneByName(123L, null);
    }

    @Test
    public void testRemoveOneByName() {
        when(mProjectRepository.removeOneByName(12345L, "project")).thenReturn(project);
        Assert.assertEquals(project, projectService.removeOneByName(12345L, "project"));
        verify(mProjectRepository).removeOneByName(12345L, "project");
    }

    @Test (expected = EmptyUserIdException.class)
    public void testUpdateProjectWithNullUserIdById() {
        projectService.updateProjectById(null, 22222L, "projectNew", "description");
    }

    @Test (expected = EmptyIdException.class)
    public void testUpdateProjectWithNullIdById() {
        projectService.updateProjectById(12345L, null, "projectNew", "description");
    }

    @Test (expected = EmptyNameException.class)
    public void testUpdateProjectWithNullNameById() {
        projectService.updateProjectById(12345L, 22222L, null, "description");
        projectService.updateProjectById(12345L, 22222L, "", "description");
    }

    @Test
    public void updateProjectById() {
        when(mProjectRepository.findOneById(12345L, 22222L)).thenReturn(project);
        doNothing().when(mProjectRepository).merge(project);
        Assert.assertEquals(
                project, projectService.updateProjectById(12345L, 22222L, "projectNew", "description")
        );
        verify(mProjectRepository).merge(project);
        verify(mProjectRepository).findOneById(12345L, 22222L);
        Assert.assertEquals(22222L, project.getId());
        Assert.assertEquals("projectNew", project.getName());
        Assert.assertEquals("description", project.getDescription());
    }

    @Test (expected = EmptyUserIdException.class)
    public void testUpdateProjectWithNullUserIdByIndex() {
        projectService.updateProjectByIndex(null, 0, "projectNew", "description");
    }

    @Test (expected = IncorrectIndexException.class)
    public void testUpdateProjectWithNullIdByIndex() {
        projectService.updateProjectByIndex(12345L, null, "projectNew", "description");
    }

    @Test (expected = EmptyNameException.class)
    public void testUpdateProjectWithNullNameByIndex() {
        projectService.updateProjectByIndex(12345L, 0, null, "description");
        projectService.updateProjectByIndex(12345L, 0, "", "description");
    }

    @Test
    public void testUpdateProjectByIndex() {
        when(mProjectRepository.findOneByIndex(12345L, 0)).thenReturn(project);
        doNothing().when(mProjectRepository).merge(project);
        @Nullable final Project expectedProject = projectService.updateProjectByIndex(
                12345L, 0, "projectNew", "description"
        );
        Assert.assertEquals(project, expectedProject);
        verify(mProjectRepository).merge(project);
        verify(mProjectRepository).findOneByIndex(12345L, 0);
        Assert.assertEquals("projectNew", project.getName());
        Assert.assertEquals("description", project.getDescription());
    }

}